## Projects

The projects folder is simply a convenient place to store projects where you know you will always have permissions to read/write. You can always save projects wherever you would like as long as you have permissions to.

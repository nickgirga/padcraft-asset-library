Samples have been recorded by Nicholas Girga using Audacity and LMMS.

Girga's Github: [github.com/nickgirga](https://gitlab.com/nickgirga)
Girga's Music: [Spotify > Bick Burga](https://open.spotify.com/artist/4C2D7ocrbmRP4xVJmOpr7I)

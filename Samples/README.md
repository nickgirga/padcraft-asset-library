## Samples

The samples folder will be used to store default and third-party audio clips used in the sampler and the pads. These are the "sounds" of your clip-based instruments.

Note: you can store your samples wherever you would like, but it is recommended that you leave the default sounds in their auto-generated folders, so that PadCraft knows where to look when it starts up.

# padcraft-asset-library

Just move the root folder into your `~/Documents` folder (or your system's "Documents" equivalent) and rename `padcraft-asset-library` to just `PadCraft`.

## Credits

Uses percussion samples from https://github.com/crabacus/the-open-source-drumkit
